FROM ubuntu:18.04

RUN apt-get update --fix-missing
RUN echo "update done doing install"
RUN apt-get install -y gawk wget git-core diffstat unzip python-pip\
 texinfo gcc-multilib build-essential chrpath socat cpio python \
 python3 python3-pip python3-pexpect xz-utils debianutils iputils-ping \
 python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev xterm locales \
 vim bash-completion screen curl

RUN wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb; dpkg -i packages-microsoft-prod.deb
RUN apt-get update

RUN echo "INSTALLING dotnet SDK 6"
RUN apt-get install -y dotnet-sdk-6.0


RUN echo "install TZDATA"
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/London
RUN apt-get install -y tzdata

RUN echo "doing Extra install"
RUN apt-get install -y autoconf libtool libglib2.0-dev libarchive-dev python-git \
 sed cvs subversion coreutils texi2html docbook-utils python-pysqlite2 \
 help2man make gcc g++ desktop-file-utils libgl1-mesa-dev libglu1-mesa-dev \
 mercurial automake groff curl lzop asciidoc u-boot-tools dos2unix mtd-utils pv \
 libncurses5 libncurses5-dev libncursesw5-dev libelf-dev zlib1g-dev bc rename rsync

ARG USERNAME=dev

ARG PUID=1000

ARG PGID=1000

RUN groupadd -g ${PGID} ${USERNAME} \
            && useradd -u ${PUID} -g ${USERNAME} -d /home/${USERNAME} ${USERNAME} \
            && mkdir /home/${USERNAME} \
            && chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}
RUN rm /usr/bin/python

RUN ln -s /usr/bin/python3 /usr/bin/python

RUN locale-gen en_US.UTF-8

ENV LANG en_US.UTF-8

COPY ./bashrc /home/${USERNAME}/.bashrc

USER ${USERNAME}

WORKDIR /home/${USERNAME}

#Install the repo tool
RUN echo "try to install repo"
RUN mkdir /home/${USERNAME}/bin
ENV PATH="/home/${USERNAME}/bin:${PATH}"
RUN curl https://storage.googleapis.com/git-repo-downloads/repo > /home/${USERNAME}//bin/repo
RUN chmod a+rx /home/${USERNAME}//bin/repo

ENV PATH="/home/${USERNAME}/.local/bin:${PATH}"
RUN echo ${PATH}
RUN echo "install azure cli"
RUN pip3 install --upgrade pip
RUN pip3 install azure-cli
RUN az config set extension.use_dynamic_install=yes_without_prompt
RUN az extension add -n azure-devops
